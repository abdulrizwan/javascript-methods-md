# JavaScript Array Methods

## 1. flat()

1. **Data types of parameter** : The flat method takes a depth parameter which is of type Number.

2. **Return value type** : It returns an array after flattening it to the specified depth.

3. **Three Examples** :
    1. `upto depth 1` : let arr = [1, [2, 3], [[4]]] console.log(arr.flat(1)) output [1, 2, 3, [4]] 
    2. `upto depth 2` : let arr = [1, [2, 3], [[4]]] console.log(arr.flat(2)) output [1, 2, 3, 4]
    3. `if no depth is provide(default is 1)` :  let arr = [1, [2, 3], [[4]]] console.log(arr.flat(1)) output [1, 2, 3, [4]] 

4. **What this method does** : This method flattens nested arrays to a array which contain values of all nested arrays.

5. **Does it mutate the original value or not** : No, It does not mutate the existing array

## 2. push()

1. **Data types of parameter** : It accepts any data type.

2. **Return value type** : It returns the same array by adding an element at the end of the array.

3. **Three Examples** :
    1. `Adding a number` : let arr = [1, 2, 3] arr.push(4) console.log(arr) // [1 2 3 4] 
    2. `Adding a string` : let arr = ['a', 'b', 'c'] arr.push('d') console.log(arr) output // ['a', 'b', 'c', 'd']
    3. `Adding an array` : let arr = [1, 2] arr.push([3, 4]) console.log(arr) output // [1, 2, [3, 4]] 

4. **What this method does** : This method accepts one or more parameters and concates them at the end of the array

5. **Does it mutate the original value or not** : Yes, It mutates the existing array

## 3. indexOf()

1. **Data types of parameter** : It accepts data type of element present in the array with optional second parameter of type number.

2. **Return value type** : It returns the index of the first parameter passed or occurence of second parameter times of value of first parameter.

3. **Three Examples** :
    let arr = [1, 2, 3, 4, 5, 2]
    1. arr.indexOf(2) returns 1 i.e., first occurence of 2.
    2. arr.indexOf(2, 2) return 5 i.e., second occurence of 2.
    3. arr.indexOf(10) return -1 as 10 is not present in the array.

4. **What this method does** : returns the index of first or custom occurence of a value in an array or returns -1

5. **Does it mutate the original value or not** : No, It does not mutate the existing array

## 4. lastIndexOf() 

1. **Data types of parameter** : It accepts data type of element present in the array.

2. **Return value type** : It returns the last index of the element if present in the array or else will provide -1.

3. **Three Examples** :
    let arr = [1, 2, 3, 4, 5, 2, 5]
    1. arr.lastindexOf(2) returns 5 i.e., last occurence of 2.
    2. arr.lastIndexOf(5) return 6 i.e., second occurence of 2.
    3. arr.indexOf(10) return -1 as 10 is not present in the array.

4. **What this method does** : returns the index of last occurence of a value in an array or returns -1

5. **Does it mutate the original value or not** : No, It does not mutate the existing array

## 5. includes() 

1. **Data types of parameter** : It accepts data type of element present in the array.

2. **Return value type** : It returns a boolean value.

3. **Three Examples** :
    1. let arr = [1, 2, 3] arr.includes(2) returns true.
    2. let arr = ['a', 'b', 'c'] arr.includes('a') returns true.
    3. let arr = [1, 2, 3] arr.includes(10) returns false.

4. **What this method does** : returns true if the element is present else returns false.

5. **Does it mutate the original value or not** : No, It does not mutate the existing array.


## 6. reverse() 

1. **Data types of parameter** : It accepts no parameter.

2. **Return value type** : returns an array with order of items being reversed.

3. **Three Examples** :
    1. let arr = [1, 2, 3] arr.reverse() console(arr) output [3, 2, 1]
    2. let arr = ['a', 'b', 'c'] arr.reverse() output ['c', 'b', 'a'].
    3. let arr = [false, true, false, 4] arr.reverse() output [4, false, true, false].

4. **What this method does** : reverse the order in which elements are arranged

5. **Does it mutate the original value or not** : Yes, It mutates the existing array

## 7. every() 

1. **Data types of parameter** : This takes a callback function as parameter.

2. **Return value type** : returns a boolean value to verify whether every elements passes the callback function.

3. **Three Examples** :
    let callback(element) = { return element < 10 }
    1. let arr = [1, 2, 3] console.log(arr.every(callback)); output true
    2. let arr = [1, 2, 3, 11] console.log(arr.every(callback)); output flase
    3. let arr = [2, 3, 5] console.log(arr.every(isPrime)) ouptut true

4. **What this method does** : It returns true if every element passes the callback function else returns false.

5. **Does it mutate the original value or not** : No, It does not mutate the existing array


## 8. shift() 

1. **Data types of parameter** : It does not accept any parameter.

2. **Return value type** : returns an array with first element removed.

3. **Three Examples** :
    1. let arr = [1, 2, 3] arr.shift() output [2, 3]
    2. let arr = ['a', 'b', 'c'] arr.shift(); output ['b', 'c']
    3. let arr = [5, 4, 3] arr.shift(); output [4, 3]

4. **What this method does** : It removes the first element and returns the rest of the array.

5. **Does it mutate the original value or not** : Yes, It mutates the existing array.

## 9. splice() 

1. **Data types of parameter** : Takes two parameters of Data Type Number.

2. **Return value type** : changes the array by deleting few elements in it.

3. **Three Examples** :
    1. let arr = [1, 2, 3, 4, 5] console.log(arr.splice(1, 4)) output [1]
    2. let arr = ['a', 'b', 'c'] console.log(arr.splice(0, 1)) output ['b', 'c']
    3. let arr = [1, 2, 3, 4, 5] console.log(arr.splice(1, 1, '2')) output [1, '2', 3, 4, 5]

4. **What this method does** : It either deletes few elements or adds new elements based on how it is instructed.

5. **Does it mutate the original value or not** : Yes, It mutates the existing array.

## 10. find() 

1. **Data types of parameter** : It takes a callback function.

2. **Return value type** : returns the first element which passes the callback function.

3. **Three Examples** :
    let callback = (element) => { return element > 25}
    1. let arr = [1, 2, 3, 4, 5] console.log(arr.find(callback)) returns undefined
    2. let arr = [1, 2, 3, 30] console.log(arr.find(callback)) returns 30
    3. let arr = [1, 26, 50, 44] console.log(arr.splice(1, 1, '2')) returns 26

4. **What this method does** : It returns the first element that passes the callback function.

5. **Does it mutate the original value or not** : No, It does not mutate the existing array.


## 11. unshift() 

1. **Data types of parameter** : It accepts any data type.

2. **Return value type** : returns the lenth of new array.

3. **Three Examples** :
    
    1. let arr = [1, 2, 3] console.log(arr.unshift(4, 5)) returns 5 but arr = [4, 5, 1, 2, 3]
    2. let arr = [1, 2, 3, 30] console.log(arr.unshift(22)) returns 5 but arr = [22, 1, 2, 3, 30]
    3. let arr = [1, 26, 50, 44] console.log(arr.unshift('a', 'b', 'c')) returns 7 but arr = ['a', 'b', 'c', 1, 26, 50, 44]

4. **What this method does** : it adds parameter at the start of the arr and returns the length of the new array.

5. **Does it mutate the original value or not** : Yes, It mutates the existing array.


## 12. findIndex() 

1. **Data types of parameter** : It takes a callback function.

2. **Return value type** : returns the index of first element which passes the callback function.

3. **Three Examples** :
    let callback = (element) => { return element > 25}
    1. let arr = [1, 2, 3, 4, 5] console.log(arr.find(callback)) returns -1
    2. let arr = [1, 2, 3, 30] console.log(arr.find(callback)) returns 3
    3. let arr = [1, 26, 50, 44] console.log(arr.splice(1, 1, '2')) returns 1

4. **What this method does** : It returns the index of first element that passes the callback function else returns -1.

5. **Does it mutate the original value or not** : No, It does not mutate the existing array.

## 13. filter() 

1. **Data types of parameter** : It takes a callback function.

2. **Return value type** : returns an array.

3. **Three Examples** :
    let callback = (element) => { return element > 25}
    1. let arr = [1, 2, 3, 4, 5] console.log(arr.filter(callback)) returns []
    2. let arr = [1, 2, 3, 30] console.log(arr.filter(callback)) returns [30]
    3. let arr = [1, 26, 50, 44] console.log(arr.filter(callback)) returns [26, 50, 44]

4. **What this method does** : returns an array elements which passes the callback function

5. **Does it mutate the original value or not** : No, It does not mutate the existing array.

## 14. forEach() 

1. **Data types of parameter** : It takes a callback function.

2. **Return value type** : Executes the callback function for every element.

3. **Three Examples** :
    let callback = (element) => { if(element>25) { console.log(element) } }
    1. let arr = [1, 2, 3, 4, 5] arr.forEach(callback) ouput 
    2. let arr = [1, 2, 3, 30] arr.forEach(callback) output 30
    3. let arr = [1, 26, 50, 44] arr.forEach(callback) output 26 50 44

4. **What this method does** : It calls the callback function for every element.

5. **Does it mutate the original value or not** : No, It does not mutate the existing array.


## 15. map() 

1. **Data types of parameter** : It takes a callback function.

2. **Return value type** : It returns a new array of same size as parent array.

3. **Three Examples** :
    let callback = (element) => { return element*element }
    1. let arr = [1, 2, 3, 4, 5] mappedArr = arr.map(callback) mappedArr = [1, 2, 9, 16, 25] 
    2. let arr = [1, 2, 3, 30] mappedArr = arr.map(callback) mappedArr = [1, 4, 9, 900] 
    3. let arr = [1, 26, 50, 44] mappedArr = arr.map(callback) mappedArr = [1, 26*26, 50*50, 44*44]

4. **What this method does** : It returns a new array with elements mapped according to callback function.

5. **Does it mutate the original value or not** : No, It does not mutate the existing array.



## 16. pop() 

1. **Data types of parameter** : It does not accept any parameter.

2. **Return value type** : It returns a last element in the array.

3. **Three Examples** :
    1. let arr = [1, 2, 3, 4, 5] popedElement = arr.pop() console.log(popedElement) // 5 arr = [1, 2, 3, 4] 
    2. let arr = ['a', 'b', 'c'] popedElement = arr.pop() console.log(popedElement) // 'c' arr = ['a', 'b'] 
    3. let arr = [1, 26, 50, 44] popedElement = arr.pop() console.log(popedElement) // 44 arr = [1, 26, 50,]

4. **What this method does** : It returns the last element of an array.

5. **Does it mutate the original value or not** : Yes, It mutates the existing array.

## 17. reduce() 

1. **Data types of parameter** : It takes a call back function and a parameter accumulator of data type number, string.

2. **Return value type** : It returns an accumulator which is decided by callback function.

3. **Three Examples** :
    const callback = (accumulator, currentValue) => accumulator + currentValue;
    1. let arr = [1, 2, 3, 4, 5] console.log(arr.reduce(callback)) output 15
    2. let arr = [4, 6, 16] console.log(arr.reduce(callback)) output 26
    3. let arr = [1, 26, 50] console.log(arr.reduce(callback)) output 77

4. **What this method does** : It returns the accumulator based on the operations of callback functions

5. **Does it mutate the original value or not** : No, It does not mutate the existing array.


## 18. slice() 

1. **Data types of parameter** : It takes two parameters of data type number.

2. **Return value type** : returns an array.

3. **Three Examples** :
    1. let arr = [1, 2, 3, 4, 5] let newArr = arr.slice(1, 3) newArr = [2, 3]
    2. let arr = [3, 4, 5, 6, 10] let newArr = arr.slice(2) newArr = [5, 6, 10]
    3. let arr = ['a', 'b', 'c'] let newArr = arr.slice(0, 2) newArr = ['a', 'b']

4. **What this method does** : It returns a shallow copy of subarray based on the parameters

5. **Does it mutate the original value or not** : No, It does not mutate the existing array.


## 19. some() 

1. **Data types of parameter** : It takes a callback function.

2. **Return value type** : returns a boolean value.

3. **Three Examples** :
    let even = (number) => { return number%2 ===0 }
    1. let arr = [1, 2, 3, 4, 5] console.log(arr.some(callback)) output true
    2. let arr = [3, 1, 5, 7, 11] console.log(arr.some(callback)) output false
    3. let arr = [3, 4] console.log(arr.some(callback)) output true

4. **What this method does** : It returns a boolean value true if some element in the array passes the callback function.

5. **Does it mutate the original value or not** : No, It does not mutate the existing array.






<!-- Practice it by yourself in console (2-3 times to understand)
Data types of parameters
Return value type
Write three examples
In your words what this method does.
Does it mutate the original value or not (check https://doesitmutate.xyz/) -->

<!-- flat
push
indexOf
lastIndexOf
includes
reverse
every
shift
splice
find
unshift
findIndex
filter
forEach
map
pop
reduce
slice
some -->